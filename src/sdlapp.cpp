#include "sdlapp.h"
#include "input.h"
#include <algorithm>
#include <cassert>
#include <cstdio>
#include <cstdarg>
//#include <chrono>
//#include <thread>
#include <imgui/imgui.h>
#include <imgui/imgui_impl_sdl.h>
#include <imgui/imgui_impl_opengl3.h>

#if defined( __WIN32__ ) || defined( _WIN32 ) || defined( WIN32 )
#include <GL/glew.h>
#else
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>
#endif // _WIN32


CSdlApp* CSdlApp::_sdlApp = nullptr;
const char errorFmt[] = "Error: %s";

static void setSdlIcon(SDL_Window* win)
{
    SDL_Surface* surface;
    Uint16 pixels[16 * 16] = {
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0aab, 0x0789, 0x0bcc, 0x0eee, 0x09aa, 0x099a, 0x0ddd,
        0x0fff, 0x0eee, 0x0899, 0x0fff, 0x0fff, 0x1fff, 0x0dde, 0x0dee,
        0x0fff, 0xabbc, 0xf779, 0x8cdd, 0x3fff, 0x9bbc, 0xaaab, 0x6fff,
        0x0fff, 0x3fff, 0xbaab, 0x0fff, 0x0fff, 0x6689, 0x6fff, 0x0dee,
        0xe678, 0xf134, 0x8abb, 0xf235, 0xf678, 0xf013, 0xf568, 0xf001,
        0xd889, 0x7abc, 0xf001, 0x0fff, 0x0fff, 0x0bcc, 0x9124, 0x5fff,
        0xf124, 0xf356, 0x3eee, 0x0fff, 0x7bbc, 0xf124, 0x0789, 0x2fff,
        0xf002, 0xd789, 0xf024, 0x0fff, 0x0fff, 0x0002, 0x0134, 0xd79a,
        0x1fff, 0xf023, 0xf000, 0xf124, 0xc99a, 0xf024, 0x0567, 0x0fff,
        0xf002, 0xe678, 0xf013, 0x0fff, 0x0ddd, 0x0fff, 0x0fff, 0xb689,
        0x8abb, 0x0fff, 0x0fff, 0xf001, 0xf235, 0xf013, 0x0fff, 0xd789,
        0xf002, 0x9899, 0xf001, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0xe789,
        0xf023, 0xf000, 0xf001, 0xe456, 0x8bcc, 0xf013, 0xf002, 0xf012,
        0x1767, 0x5aaa, 0xf013, 0xf001, 0xf000, 0x0fff, 0x7fff, 0xf124,
        0x0fff, 0x089a, 0x0578, 0x0fff, 0x089a, 0x0013, 0x0245, 0x0eff,
        0x0223, 0x0dde, 0x0135, 0x0789, 0x0ddd, 0xbbbc, 0xf346, 0x0467,
        0x0fff, 0x4eee, 0x3ddd, 0x0edd, 0x0dee, 0x0fff, 0x0fff, 0x0dee,
        0x0def, 0x08ab, 0x0fff, 0x7fff, 0xfabc, 0xf356, 0x0457, 0x0467,
        0x0fff, 0x0bcd, 0x4bde, 0x9bcc, 0x8dee, 0x8eff, 0x8fff, 0x9fff,
        0xadee, 0xeccd, 0xf689, 0xc357, 0x2356, 0x0356, 0x0467, 0x0467,
        0x0fff, 0x0ccd, 0x0bdd, 0x0cdd, 0x0aaa, 0x2234, 0x4135, 0x4346,
        0x5356, 0x2246, 0x0346, 0x0356, 0x0467, 0x0356, 0x0467, 0x0467,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff,
        0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff, 0x0fff
    };
    surface = SDL_CreateRGBSurfaceFrom(pixels, 16, 16, 16, 16 * 2, 0x0f00, 0x00f0, 0x000f, 0xf000);
    SDL_SetWindowIcon(win, surface);
    SDL_FreeSurface(surface);
}

static SDL_Window* createSdlWindow(SDL_GLContext* ctx = nullptr)
{
    SDL_Window* sdlwindow = SDL_CreateWindow("SDL",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        320, 200,
        SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_HIDDEN);
    if (!sdlwindow) {
        SDL_Log(errorFmt, SDL_GetError());
        exit(1);
    }

    if (ctx)
    {
        SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
        SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

        SDL_GLContext glcontext = SDL_GL_CreateContext(sdlwindow);
        if (!glcontext) {
            SDL_Log(errorFmt, SDL_GetError());
            exit(1);
        }
        *ctx = glcontext;
        SDL_GL_MakeCurrent(sdlwindow, glcontext);
    }

    return sdlwindow;
}

static void initOpenGLFunctions()
{
#ifdef _WIN32
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        SDL_Log(errorFmt, glewGetErrorString(err));
        exit(1);
    }
#endif // _WIN32
}


CSdlApp::CSdlApp(int argc, char* argv[])
    : CActive{ false }
    , m_input{ new CInput() }
{
    if (_sdlApp != nullptr)
    {
        fprintf(stderr, "Error: there can be only one CSdlApp!\n");
        exit(1);
    }

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
    {
        fprintf(stderr, "Error: %s\n", SDL_GetError());
        exit(1);
    }

    if (hasOpenGL())
    {
        m_window.m_win = createSdlWindow(&m_window.m_glcontext);
        initOpenGLFunctions();

        const char* glsl_version = "#version 330 core";
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

        // Setup Dear ImGui style
        ImGui::StyleColorsDark();
        //ImGui::StyleColorsClassic();

        // Setup Platform/Renderer backends
        ImGui_ImplSDL2_InitForOpenGL(m_window.m_win, m_window.m_glcontext);
        ImGui_ImplOpenGL3_Init(glsl_version);
    }
    else
    {
        m_window.m_win = createSdlWindow(nullptr);
    }

    setSdlIcon(m_window.m_win);
    _sdlApp = this;
    activeRegister(this);
}

CSdlApp::~CSdlApp()
{
    _sdlApp = nullptr;
    if (m_window.m_glcontext)
    {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplSDL2_Shutdown();
        ImGui::DestroyContext();
        SDL_GL_DeleteContext(m_window.m_glcontext);
    }
    SDL_DestroyWindow(m_window.m_win);
    log("SDL_Quit()");
    SDL_Quit();
}

void CSdlApp::run()
{
    Uint32 curr_tick = 0;
    Uint32 last_tick = SDL_GetTicks();

    while (!m_quit)
    {
        curr_tick = SDL_GetTicks();
        m_delta = float(curr_tick - last_tick) / 1000.0f;
        last_tick = curr_tick;

        SDL_Event e;
        while (SDL_PollEvent(&e))
        {
            ImGui_ImplSDL2_ProcessEvent(&e);

            if (e.type == SDL_QUIT)
                m_quit = true;
            if (e.type == SDL_WINDOWEVENT
                && e.window.event == SDL_WINDOWEVENT_CLOSE
                && e.window.windowID == SDL_GetWindowID(m_window.m_win))
                m_quit = true;

            if (m_quit) goto quit;

            if (e.type == SDL_WINDOWEVENT && e.window.windowID == SDL_GetWindowID(m_window.m_win))
                handleWindowEvent(e);
            handleInput(e);
            handleEvent(e);     // virtual

            if (m_quit) goto quit;
        }

        // process all registered "active" objects
	    for (auto active : m_active) {
		    active->process();
	    }

        idle();     // virtual

        m_window.m_resized = false;
        m_ticks++;
    }

quit:
    return;
}

void CSdlApp::handleEvent(SDL_Event& e)
{
}

void CSdlApp::idle()
{
    if (!m_window.m_focus)
    {
        SDL_Delay(5);
        //std::this_thread::sleep_for(std::chrono::milliseconds(5));
    }
}

void CSdlApp::log(const std::string fmt, ...)
{
    std::vector<char> buff(4096);
    va_list args;
    va_start(args, fmt);
    vsprintf_s(buff.data(), 4095, fmt.c_str(), args);
    va_end(args);
    SDL_Log("%s", buff.data());
}

void CSdlApp::activeRegister(CActive* item)
{
#if defined(_DEBUG)
    assert(std::find(m_active.begin(), m_active.end(), item) == m_active.end());
#endif
    if (item) m_active.push_back(item);
}

void CSdlApp::activeUnregister(CActive* item)
{
	if (item) m_active.remove(item);
}

void CSdlApp::handleInput(SDL_Event& e)
{
    switch (e.type)
    {
    case SDL_KEYDOWN:
        input().keyEvent(e.key.keysym.scancode, true);
        break;

    case SDL_KEYUP:
        input().keyEvent(e.key.keysym.scancode, false);
        break;

    case SDL_MOUSEBUTTONDOWN:
        input().buttonEvent(e.button.button, true);
        break;

    case SDL_MOUSEBUTTONUP:
        input().buttonEvent(e.button.button, false);
        break;

    case SDL_MOUSEMOTION:
        if (SDL_GetRelativeMouseMode())
            input().motionEvent(e.motion.xrel, e.motion.yrel);
        else
            input().motionEvent(0, 0);
        break;

    case SDL_MOUSEWHEEL:
        break;
    }
}

void CSdlApp::handleWindowEvent(SDL_Event& e)
{
    int w, h;
    switch (e.window.event)
    {
    case SDL_WINDOWEVENT_SHOWN:
        log("Window %d shown", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_HIDDEN:
        log("Window %d hidden", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_EXPOSED:
        log("Window %d exposed", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_MOVED:
        log("Window %d moved to %d,%d",
            e.window.windowID, e.window.data1,
            e.window.data2);
        break;

    case SDL_WINDOWEVENT_RESIZED:
        log("Window %d resized to %dx%d",
            e.window.windowID, e.window.data1,
            e.window.data2);
        w = e.window.data1;
        h = e.window.data2;
        m_window.m_width = w;
        m_window.m_height = h;
        m_window.m_resized = true;
        if (hasOpenGL())
            glViewport(0, 0, w, h);
        break;

    case SDL_WINDOWEVENT_SIZE_CHANGED:
        log("Window %d size changed to %dx%d",
            e.window.windowID, e.window.data1,
            e.window.data2);
        w = e.window.data1;
        h = e.window.data2;
        m_window.m_width = w;
        m_window.m_height = h;
        m_window.m_resized = true;
        if (hasOpenGL())
            glViewport(0, 0, w, h);
        break;

    case SDL_WINDOWEVENT_MINIMIZED:
        log("Window %d minimized", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_MAXIMIZED:
        log("Window %d maximized", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_RESTORED:
        log("Window %d restored", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_ENTER:
        log("Mouse entered window %d", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_LEAVE:
        log("Mouse left window %d", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_FOCUS_GAINED:
        m_window.m_focus = true;
        log("Window %d gained keyboard focus", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_FOCUS_LOST:
        m_window.m_focus = false;
        log("Window %d lost keyboard focus", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_CLOSE:
        log("Window %d closed", e.window.windowID);
        break;

#if SDL_VERSION_ATLEAST(2, 0, 5)
    case SDL_WINDOWEVENT_TAKE_FOCUS:
        log("Window %d is offered a focus", e.window.windowID);
        break;

    case SDL_WINDOWEVENT_HIT_TEST:
        log("Window %d has a special hit test", e.window.windowID);
        break;
#endif

    default:
        log("Window %d got unknown event %d",
            e.window.windowID, e.window.event);
        break;
    }
}

std::ifstream CSdlApp::io::textInput(const std::string& filename)
{
    std::ifstream ifs(filename);
    if (!ifs)
    {
        log("Cannot open file: %s", filename.c_str());
        std::exit(17);
    }

    return ifs;
}

std::ifstream CSdlApp::io::binaryInput(const std::string& filename)
{
    std::ifstream ifs(filename, std::ios::binary);
    if (!ifs)
    {
        log("Cannot open file: %s", filename.c_str());
        std::exit(17);
    }

    return ifs;
}

std::ofstream CSdlApp::io::binaryOutput(const std::string& filename)
{
    std::ofstream ofs(filename, std::ios::binary);
    if (!ofs)
    {
        log("Cannot open file: %s", filename.c_str());
        std::exit(18);
    }

    return ofs;
}
