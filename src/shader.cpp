#include "shader.h"
#include "sdlapp.h"
#include <cassert>
#include <cstdlib>
#include <string>
#include <vector>


static const char errorFmt[] = { "%s\n" };

void CShader::unbind()
{
	glUseProgram(0);
}

CShader::CShader(unsigned int prg)
	: m_prg{ prg }
{
}

CShader::CShader(const std::string& vertex, const std::string& fragment)
{
	init(vertex, fragment);
}

CShader::~CShader()
{
	if (m_prg) glDeleteProgram(m_prg);
}

void CShader::init(const std::string& vertex, const std::string& fragment)
{
	if (!_init(vertex, fragment))
	{
		CSdlApp::log(errorFmt, "Cannot init shader program, quit!");
		std::exit(10);
	}
}

bool CShader::_init(const std::string& vertex, const std::string& fragment)
{
	unsigned int vsh{}, fsh{}, prg{};
	bool result{ false };

	vsh = glCreateShader(GL_VERTEX_SHADER);
	if (!_compile(vsh, vertex)) goto exit1;

	fsh = glCreateShader(GL_FRAGMENT_SHADER);
	if (!_compile(fsh, fragment)) goto exit2;

	prg = glCreateProgram();
	result = _link(prg, vsh, fsh);
	if (!result) goto exit3;

	if (m_prg) glDeleteProgram(m_prg);
	m_prg = prg;
	goto exit0;

exit3:
	glDeleteProgram(prg);
exit2:
	glDeleteShader(fsh);
exit1:
	glDeleteShader(vsh);
exit0:
	return result;
}

bool CShader::_compile(unsigned int id, const std::string& source) const
{
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);
	return _checkStatus(id, EStatusType::COMPILE);
}

bool CShader::_link(unsigned int prg, unsigned int vid, unsigned int fid) const
{
	glAttachShader(prg, vid);
	glAttachShader(prg, fid);
	glLinkProgram(prg);
	return _checkStatus(prg, EStatusType::LINK);
}

bool CShader::_checkStatus(unsigned int id, EStatusType type) const
{
	int params{};
	unsigned int status{};
	void (*pGet)(unsigned int, unsigned int, int*) { glGetProgramiv };
	void (*pGetInfoLog)(unsigned int, int, int*, char*) { glGetProgramInfoLog };

	switch (type)
	{
	case EStatusType::COMPILE:
		status = GL_COMPILE_STATUS;
		pGet = glGetShaderiv;
		pGetInfoLog = glGetShaderInfoLog;
		break;
	case EStatusType::LINK:
		status = GL_LINK_STATUS;
		break;
	case EStatusType::VALIDATE:
		status = GL_VALIDATE_STATUS;
		break;
	default:
		assert(!"Not reached!");
		return false;
	}

	pGet(id, status, &params);

	if (params == GL_FALSE)
	{
		int buffsz{};
		pGet(id, GL_INFO_LOG_LENGTH, &buffsz);
		std::vector<char> buff(buffsz + 1);

		pGetInfoLog(id, buffsz, nullptr, buff.data());
		buff[buffsz] = '\0';
		CSdlApp::log(errorFmt, buff.data());
		return false;
	}

	return true;
}

bool CShader::validate() const
{
	if (m_prg)
	{
		glValidateProgram(m_prg);
		return _checkStatus(m_prg, EStatusType::VALIDATE);
	}
	else
	{
		assert(!"Validate null shader program!");
		return false;
	}
}
