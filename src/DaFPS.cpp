#include "myapp.h"



//{ "POSITION", 0 },
//{ "NORMAL", 1 },
//{ "TANGENT", 2 },
//{ "TEXCOORD_0", 3 },
//{ "TEXCOORD_1", 4 },
//{ "COLOR_0", 5 },
//{ "COLOR_1", 6 },



int main(int argc, char *argv[])
{
    CMyApp app(argc, argv);
    app.run();
    return 0;
}
