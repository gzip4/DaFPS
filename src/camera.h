#pragma once
#ifndef _DAFPS_CAMERA_H
#define _DAFPS_CAMERA_H

#include "active.h"
#include <SDL2/SDL.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class CCamera : public CActive
{
protected:
	glm::vec3 pos;
	glm::vec3 rot;

public:
	virtual void handleEvent(SDL_Event& event) {}
	virtual void process() override {}

	void moveTo(float x, float y, float z) { pos = glm::vec3(x, y, z); }
	void moveTo(const glm::vec3& v) { pos = v; }
	void move(float x, float y, float z) { pos += glm::vec3(x, y, z); }
	void move(const glm::vec3& v) { pos += v; }
	void move(const glm::vec2& v) { pos += glm::vec3(v, 0.0f); }
	void rotateTo(float x, float y, float z) { rot = glm::vec3(x, y, z); }
	void rotateTo(const glm::vec3& v) { rot = v; }
	void rotate(float x, float y, float z) { rot += glm::vec3(x, y, z); }
	void rotate(const glm::vec3& v) { rot += v; }

	glm::vec3 position() const { return -pos; }
	glm::vec3 rotation() const { return rot; }
	glm::mat4 rotationMatrix() const;
	glm::mat4 matrix() const;
};

class CFlyCamera : public CCamera
{
protected:
	float m_maxSpeed{};
	float m_xsensitivity{ 0.001f };
	float m_ysensitivity{ 0.001f };

public:
	explicit CFlyCamera(float maxSpeed = 1.0f)
		: CCamera()
		, m_maxSpeed{ maxSpeed }
	{
	}
	virtual void handleEvent(SDL_Event& event) override;
	virtual void process() override;

	float& pitch() { return rot.x; }
	float& yaw() { return rot.y; }
	float& roll() { return rot.z; }
	float& maxSpeed() { return m_maxSpeed; }
	float& xsensitivity() { return m_xsensitivity; }
	float& ysensitivity() { return m_ysensitivity; }
};

class CFlyCamera2 : public CFlyCamera
{
protected:
	float m_accel{};
	glm::vec3 m_vel{};

public:
	virtual void process() override;

	float& accel() { return m_accel; }
	glm::vec3 velocity() const { return m_vel; }
};

#endif // !_DAFPS_CAMERA_H
