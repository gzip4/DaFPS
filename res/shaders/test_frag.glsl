//FRAGMENT SHADER
#version 330 core

in vec4 fragPos;
in vec3 normalVector;
in vec2 texCoord0;
in vec4 color0;

out vec4 fragColor;

///////////////////////////////////////////////////////////////////////////
struct Light {
    vec3 position;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform int shadingMode = 1;
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform float fShininess = 32.0;
uniform float delta = 0.001;

uniform vec3 viewPos = vec3(0,2,5);
uniform Light light = Light(
    vec3(50,100,0),
    vec3(0.2,0.2,0.2),
    vec3(1,1,1),
    vec3(1,1,1));


///////////////////////////////////////////////////////////////////////////
vec3 phongShading() {
    // ambient
    vec3 ambient = light.ambient * texture(diffuseTexture, texCoord0).rgb
        + light.ambient * color0.rgb;

    // diffuse
    vec3 norm = normalize(normalVector);
    vec3 lightDir = normalize(light.position - fragPos.xyz);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(diffuseTexture, texCoord0).rgb;
    vec3 vColor = light.diffuse * diff * color0.rgb;

    // specular
    vec3 viewDir = normalize(viewPos - fragPos.xyz);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), fShininess);
    vec3 specular = light.specular * spec;// * texture(specularTexture, texCoord0).rgb;

    vec3 result = ambient + diffuse + specular + vColor;
    return result;
}

///////////////////////////////////////////////////////////////////////////
vec3 blinnPhongShading() {
    // ambient
    vec3 ambient = light.ambient * texture(diffuseTexture, texCoord0).rgb
        + light.ambient * color0.rgb;

    // diffuse
    vec3 norm = normalize(normalVector);
    vec3 lightDir = normalize(light.position - fragPos.xyz);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = light.diffuse * diff * texture(diffuseTexture, texCoord0).rgb;
    vec3 vColor = light.diffuse * diff * color0.rgb;

    // specular
    vec3 viewDir = normalize(viewPos - fragPos.xyz);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(norm, halfwayDir), 0.0), fShininess);
    vec3 specular = light.specular * spec;// * texture(specularTexture, texCoord0).rgb;

    vec3 result = ambient + diffuse + specular + vColor;
    return result;
}

///////////////////////////////////////////////////////////////////////////
void main() {
    switch (shadingMode) {
    case 0:
        fragColor = vec4(phongShading(), 1);
        break;
    case 1:
        fragColor = vec4(blinnPhongShading(), 1);
        break;
    default:
        fragColor = vec4(0,0,0,1);
    }
}
