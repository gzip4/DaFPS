#include "scene.h"
#include "sdlapp.h"
#include "buffers.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>


void CScene::drawScene(Func1 beforeNode, Func2 beforePrimitive) const
{
    for (auto& pNode : m_nodes)
    {
        beforeNode(pNode.get());

        auto& pMesh = m_meshes[pNode->meshIndex()];
        for (auto& pPrim : pMesh->primitives())
        {
            if (!pPrim->indexed()) continue;

            beforePrimitive(pPrim.get());
            pPrim->drawPrimitive();
        }
    }
}

void
CScene::loadNode(uint32_t inode, const Document& document, CNode* parent)
{
    auto& node = document.nodes[inode];
    std::unique_ptr<CNode> pNode(new CNode(node.name));
    pNode->m_meshIndex = node.mesh;

    pNode->translation() = glm::vec3(
        node.translation[0],
        node.translation[1],
        node.translation[2]);

    pNode->rotation() = glm::quat(
        node.rotation[3],   // w
        node.rotation[0],   // x
        node.rotation[1],   // y
        node.rotation[2]);  // z

    pNode->scale() = glm::vec3(
        node.scale[0],
        node.scale[1],
        node.scale[2]);

    // TODO: what if matrix here?
    //auto m = node.matrix;
    //union {
    //    glm::mat4 m4;
    //    float f[16];
    //} cvt;
    //memcpy(cvt.f, m.data(), sizeof(cvt));
    //glm::mat4 m4 = cvt.m4;

    const glm::mat4 m4id(1.0f);
    const glm::mat4 m4pos = glm::translate(m4id, pNode->translation());
    const glm::mat4 m4rot = glm::mat4(pNode->rotation());
    const glm::mat4 m4scale = glm::scale(m4id, pNode->scale());

    // order: T * R * S
    pNode->matrix() = m4pos * m4rot * m4scale;

    if (parent)
    {
        pNode->matrix() = parent->matrix() * pNode->matrix();
        pNode->translation() = parent->matrix()
            * glm::vec4(pNode->translation(), 1.0f);
        pNode->scale() = parent->matrix() * glm::vec4(pNode->scale(), 1.0f);

        // TODO: check order of members: pq != qp
        pNode->rotation() = parent->rotation() * pNode->rotation();
    }

    //std::cout << "V: " << pNode->translation()
    //    << " " << pNode->rotation() << std::endl;
    //std::cout << "MAT4: " << pNode->matrix() << std::endl;

    for (int32_t ichild : node.children)
    {
		(void)loadNode(ichild, document, pNode.get());
    }

    // no mesh - no render, no render - no storage
    if (node.mesh >= 0)
    {
        m_nodes.push_back(std::move(pNode));       // add node to scene
    }
}

void
CScene::loadMesh(int32_t imesh, const Document& document, Buffers& buffers)
{
    const auto& mesh = document.meshes[imesh];
    std::unique_ptr<CMesh> pMesh(new CMesh(mesh.name));
    auto& attrLocMap = attributeLocations();

    for (const auto& prim : mesh.primitives)
    {
        std::unique_ptr<CPrimitive> p1(new CPrimitive);
        p1->m_mode = uint8_t(prim.mode);

        // opengl: bind vertex array
        p1->m_vao.bind();

        // iterate for attributes (position, normals, UVs...)
        for (const auto& attr : prim.attributes)
        {
            const std::string& attrName = attr.first;
            const uint32_t attrAccessor = attr.second;
            const auto& accessor = document.accessors[attrAccessor];

            if (accessor.bufferView >= 0)
            {
                const auto& bview = document.bufferViews[accessor.bufferView];
                const uint8_t size = uint8_t(accessor.type);    // 1..4

                const auto attrLoc = attrLocMap.find(attrName);
                if (attrLoc != attrLocMap.end())
                {
                    //const std::string& attrName = attrLoc->first;
                    const uint8_t attrLocation = attrLoc->second;

                    // opengl: bind buffer to vertex array
                    buffers[bview.buffer]->bind();

					using CT = fx::gltf::Accessor::ComponentType;
					if (accessor.componentType == CT::Float)
					{
						p1->m_vao.attrV(attrLocation, size,
							bview.byteStride, bview.byteOffset);
					}
					else if (accessor.componentType == CT::UnsignedShort
						|| accessor.componentType == CT::UnsignedByte
						|| accessor.componentType == CT::UnsignedInt)
					{
						p1->m_vao.attrColor(attrLocation,
							unsigned(accessor.componentType),
							bview.byteStride,
							bview.byteOffset);
					}
					else
					{
						CSdlApp::log("Unsupported component type: %d",
							accessor.componentType);
						exit(14);
					}

                    p1->m_attribs.push_back(attrLocation);
                }
                else
                {
                    CSdlApp::log("Unknown attribute type: %s",
                        attrName.c_str());
                }
            }
        }

        if (prim.indices >= 0)
        {
            using CT = fx::gltf::Accessor::ComponentType;
            const auto& accessor = document.accessors[prim.indices];
            p1->m_componentType = uint16_t(accessor.componentType);
            const auto& bview = document.bufferViews[accessor.bufferView];
            p1->m_indexed = true;
            p1->m_indexCount = accessor.count;
            p1->m_indexOffset = bview.byteOffset;

            // opengl: bind element index to vertex array
            buffers[bview.buffer]->bind(CBuffer::Index);
        }

        pMesh->m_primitives.push_back(std::move(p1));
    }

    m_meshes.push_back(std::move(pMesh));
}

void CScene::loadScene(const Document& document)
{
    m_nodes.clear();
    m_meshes.clear();

    // opengl: create all scene data buffers
    Buffers buffers;
    for (const auto& b : document.buffers)
    {
        CBuffer* bb = new CBuffer;
        bb->data(b.data);
        buffers.push_back(std::unique_ptr<CBuffer>(bb));
    }

    const auto defaultScene = document.scene;
    for (uint32_t i : document.scenes[defaultScene].nodes)
    {
		loadNode(i, document, nullptr);
    }

    for (int32_t i = 0; i < document.meshes.size(); i++)
    {
        loadMesh(i, document, buffers);
    }

    // opengl: unbind buffers used
    CBuffer::unbind();
    CVertexArray::unbind();
}

CScene::CNode::CNode(const std::string& name)
    : m_name(name)
{
    CSdlApp::log("Create node: %s", name.c_str());
}

CScene::CNode::~CNode()
{
    CSdlApp::log("Destroy node: %s", m_name.c_str());
}

CScene::CMesh::CMesh(const std::string& name)
    : m_name(name)
{
    CSdlApp::log("Create mesh: %s", name.c_str());
}

CScene::CMesh::~CMesh()
{
    CSdlApp::log("Destroy mesh: %s", m_name.c_str());
}

void CScene::CPrimitive::drawPrimitive()
{
    vao().bind();
    vao().enableAttr(attribs());
    glDrawElements(mode(), indexCount(), componentType(),
        (void*)indexOffset());
    vao().disableAttr(attribs());
}
