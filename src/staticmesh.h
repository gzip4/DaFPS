#pragma once
#ifndef _DAFPS_STATICMESH_H
#define _DAFPS_STATICMESH_H

#include "buffers.h"
#include <vector>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

class CStaticMesh
{
public:
	enum LOD
	{
		LOD0 = 0,
		LOD1 = 1,
		LOD2 = 2,
		LOD3 = 3,
		LOD4 = 4,
		LOD5 = 5,
		LOD6 = 6,
		LOD7 = 7,
		MAX_LOD = 8
	};

protected:
	glm::vec3 m_pos;
	glm::vec3 m_rot;
	glm::vec3 m_scale;

	CVertexArray m_vao;
	std::vector<CVertexArray> m_lod;

	// if changed, kepp initAttrib() in sync
	struct SAttr
	{
		glm::vec3 pos;
		glm::vec3 normal;
		glm::vec3 color;
		glm::vec2 uv0;
		glm::vec2 uv1;
	};

public:
	CStaticMesh(CStaticMesh const&) = delete;
	CStaticMesh& operator=(CStaticMesh const&) = delete;
	CStaticMesh();
	virtual ~CStaticMesh();
	virtual void init();

	glm::vec3& position() { return m_pos; }
	glm::vec3& rotation() { return m_rot; }
	glm::vec3& scale() { return m_scale; }


protected:
	void initAttrib(CVertexArray const&);
};

#endif // !_DAFPS_STATICMESH_H
