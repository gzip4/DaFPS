#include "active.h"
#include "sdlapp.h"

CActive::CActive(bool reg)
	: m_reg{ reg }
{
	if (reg) CSdlApp::app().activeRegister(this);
}

CActive::~CActive()
{
	if (m_reg) CSdlApp::app().activeUnregister(this);
}
