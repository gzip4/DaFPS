#include "myapp.h"
#include "genv.h"
#include "wrapgl.h"
#include <fx/gltf.h>

#include <imgui/imgui.h>
#include <imgui/imgui_impl_sdl.h>
#include <imgui/imgui_impl_opengl3.h>

// 0:off, 1:on
#define RELATIVE_MOUSE_DEFAULT 0

#ifdef _WIN32
//#define WIN32_LEAN_AND_MEAN
//#include <windows.h>
#define SW_HIDE 0
#define SW_SHOWNOACTIVATE 4
#define SW_SHOW 5
#define SW_SHOWNA 8
typedef void* HWND;
extern "C" {
    __declspec(dllimport) HWND __stdcall GetConsoleWindow();
    __declspec(dllimport) int __stdcall ShowWindow(HWND, int);
}
#endif // _WIN32

_genv_s genv;



static std::vector<glm::vec3> tri()
{
    using namespace glm;
    return {
        vec3(-1.0f, 1.0f, 0.0f),
        vec3(-1.0f, -1.0f, 0.0f),
        vec3(1.0f, -1.0f, 0.0f),
        vec3(-1.0f, 1.0f, 0.0f),
        vec3(1.0f, 1.0f, 0.0f),
        vec3(1.0f, -1.0f, 0.0f)
    };
}

static std::vector<glm::vec2> triTex()
{
    using namespace glm;
    return {
        vec2(0.0f, 1.0f),
        vec2(0.0f, 0.0f),
        vec2(1.0f, 0.0f),
        vec2(0.0f, 1.0f),
        vec2(1.0f, 1.0f),
        vec2(1.0f, 0.0f)
    };
}

void GLAPIENTRY
MessageCallback(GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar* message,
    const void* userParam)
{
    //CSdlApp::log("GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
    //    (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
    //    type, severity, message);
    if (type == GL_DEBUG_TYPE_ERROR)
    {
        CSdlApp::log("GL CALLBACK: type = 0x%x, severity = 0x%x, message = %s\n",
            type, severity, message);
    }
}


CMyApp::CMyApp(int argc, char* argv[])
    : CSdlApp{ argc, argv }
    , CSdlAppInit{ NEED_OPENGL }
{
#ifdef _WIN32
    //ShowWindow(GetConsoleWindow(), SW_HIDE);
#endif // _WIN32

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(MessageCallback, 0);

    s1.init(Resource(RES_SHADER_TEST_VERT).str(), Resource(RES_SHADER_TEST_FRAG).str());
    s2.init(Resource(RES_SHADER_TEXTURE_VERT).str(), Resource(RES_SHADER_TEXTURE_FRAG).str());

    log("Number of logical CPU cores: %d", SDL_GetCPUCount());
    log("System memory: %d Mb", SDL_GetSystemRAM());
    log("OpenGL %s [%s] [%s]",
        glGetString(GL_VERSION),
        glGetString(GL_VENDOR),
        glGetString(GL_RENDERER));

    {
        int r, g, b;
        SDL_GL_GetAttribute(SDL_GL_RED_SIZE, &r);
        SDL_GL_GetAttribute(SDL_GL_GREEN_SIZE, &g);
        SDL_GL_GetAttribute(SDL_GL_BLUE_SIZE, &b);
        log("RGB bits: %d %d %d\n", r, g, b);
    }

    SDL_GL_SetSwapInterval(VSYNC_ON);
    //SDL_GL_SetSwapInterval(VSYNC_OFF);
    SDL_SetWindowTitle(window().handle(), "SDL/2.0");
    SDL_SetWindowSize(window().handle(), WIN_W, WIN_H);
    SDL_SetWindowPosition(window().handle(),
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED);
    SDL_ShowWindow(window().handle());
    {
        int w, h;
        SDL_GL_GetDrawableSize(window().handle(), &w, &h);
        glViewport(0, 0, w, h);
    }
#if (RELATIVE_MOUSE_DEFAULT == 1)
    SDL_SetRelativeMouseMode(SDL_TRUE);
#endif

    gl::clearColor(glm::vec3(0.10f, 0.1f, 0.15f));
    glEnable(GL_MULTISAMPLE);

    cam1.accel() = 20.0f;
    cam1.maxSpeed() = 2.0f;
    cam1.moveTo(0.f, -2.f, -5.f);

    scene1.loadScene(fx::gltf::LoadFromBinary("res/cube.glb"));

    CBuffer b1, b2;
    buffers.vao1.bind();
    b1.data(tri());
    buffers.vao1.attrV3(0);
    b2.data(triTex());
    buffers.vao1.attrUV(1);
    buffers.vao1.unbind();

    buffers.fbo.attachColor(WIN_W, WIN_H);
    buffers.fbo.attachDepth(WIN_W, WIN_H);
    buffers.fbo.unbind();

    terrain.init(300);

    log("Load skybox texture...");
    skybox.loadCubemap({
        "res/sky/posx.dds", "res/sky/negx.dds",
        "res/sky/posy.dds", "res/sky/negy.dds",
        "res/sky/posz.dds", "res/sky/negz.dds" });
    log("Load skybox texture...done");
}

CMyApp::~CMyApp()
{
    log("CMyApp::~CMyApp()");
}

void CMyApp::handleEvent(SDL_Event& e)
{
    CSdlApp::handleEvent(e);

    if (input().keyPressed(SDL_SCANCODE_ESCAPE))
    {
        quit();
        log("QUIT!");
        return;
    }

    cam1.handleEvent(e);

    if (input().keyPressed(SDL_SCANCODE_F1))
    {
        scene1.loadScene(fx::gltf::LoadFromBinary("res\\cube.glb"));
        log("CAM: %s", util::str(cam1.matrix()).c_str());
        auto pos = cam1.position() * -1.0f;
        log("POS: %.2f %.2f %.2f", pos.x, pos.y, pos.z);
    }

    // reset camera
    if (input().keyPressed(SDL_SCANCODE_KP_5))
    {
        cam1.accel() = 20.0f;
        cam1.maxSpeed() = 2.0f;
        cam1.moveTo(0.f, -2.f, -5.f);
        cam1.pitch() = cam1.roll() = cam1.yaw() = 0.0f;
    }

    if (input().keyPressed(SDL_SCANCODE_1)) m_rt = ERenderType::Color;
    if (input().keyPressed(SDL_SCANCODE_2)) m_rt = ERenderType::Depth;
    if (input().keyPressed(SDL_SCANCODE_3)) m_rt = ERenderType::ColorDepth;

    if (input().keyPressed(SDL_SCANCODE_F2))
    {
        cam1.accel() = 20.0f;
        cam1.maxSpeed() = 2.0f;
    }
    if (input().keyPressed(SDL_SCANCODE_F3))
    {
        cam1.accel() = 200.0f;
        cam1.maxSpeed() = 20.0f;
    }
    if (input().keyPressed(SDL_SCANCODE_F4))
    {
        cam1.accel() = 2000.0f;
        cam1.maxSpeed() = 200.0f;
    }
    if (input().keyPressed(SDL_SCANCODE_F5))
    {
        cam1.accel() = 20000.0f;
        cam1.maxSpeed() = 2000.0f;
    }

    switch (e.type)
    {
#if (RELATIVE_MOUSE_DEFAULT == 1)
    case SDL_KEYDOWN:
        if (e.key.keysym.scancode == SDL_SCANCODE_LALT)
            SDL_SetRelativeMouseMode(SDL_FALSE);
        break;

    case SDL_KEYUP:
        if (e.key.keysym.scancode == SDL_SCANCODE_LALT)
            SDL_SetRelativeMouseMode(SDL_TRUE);
        break;
#else
    case SDL_MOUSEBUTTONDOWN:
        if (e.button.button == SDL_BUTTON_RIGHT)
            SDL_SetRelativeMouseMode(SDL_TRUE);
        break;

    case SDL_MOUSEBUTTONUP:
        if (e.button.button == SDL_BUTTON_RIGHT)
        {
            SDL_SetRelativeMouseMode(SDL_FALSE);
            SDL_WarpMouseInWindow(window().handle(),
                window().width() / 2,
                window().height() / 2);
        }
        break;
    }
#endif

#ifdef _WIN32
    if (input().keyPressed(SDL_SCANCODE_F12))
    {
        m_consoleVisible = !m_consoleVisible;
        ShowWindow(GetConsoleWindow(), m_consoleVisible ? SW_SHOWNOACTIVATE : SW_HIDE);
    }
#endif // _WIN32


    if (window().resized())
    {
        m_m4proj = glm::perspective(
            glm::radians(m_fov),
            window().aspect(),
            0.1f,
            10000.0f);
    }
}

void CMyApp::idle()
{
    CSdlApp::idle();
}

void CMyApp::process()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(window().handle());
    ImGui::NewFrame();


    renderToTexture();
    //renderScene(scene1);


    static bool show_demo_window = false;
    static bool show_another_window = true;

    // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
    if (show_demo_window)
        ImGui::ShowDemoWindow(&show_demo_window);

    // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
    {
        static float f = 0.0f;
        static ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

        ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.

        ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
        ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
        ImGui::Checkbox("Another Window", &show_another_window);

        ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
        ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        ImGui::End();
    }



    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());


    window().swapWindow();
    alpha += 0.01f * delta();
}

void CMyApp::drawTextures(unsigned int tex1, unsigned int tex2)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST);

    s2.use();

    buffers.vao1.bind();
    buffers.vao1.enableAttr({ 0, 1 });

    if (m_rt == ERenderType::Color)
    {
        s2.tex2d("tex1", tex1);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
    else if (m_rt == ERenderType::Depth)
    {
        s2.tex2d("tex1", tex2);
        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
    else
    {
        s2.tex2d("tex1", tex2);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        s2.tex2d("tex1", tex1);
        glDrawArrays(GL_TRIANGLES, 3, 3);
    }
}

void CMyApp::renderToTexture()
{
    if (window().resized())
    {
        // (re)attach textures to frame buffer
        int w = window().width();
        int h = window().height();
        buffers.fbo.attachColor(w, h);
        buffers.fbo.attachDepth(w, h);
    }

    // render scene to textures
    buffers.fbo.bind();
    {
        gl::clearAll();

        glm::mat4 m4projView = m4proj() * cam1.matrix();
        renderScene(scene1);
        skybox.renderSkybox(m4proj() * cam1.rotationMatrix());
        terrain.renderTerrain(m4projView);
    }
    buffers.fbo.unbind();

    drawTextures(buffers.fbo.color(), buffers.fbo.depth());
}

void CMyApp::renderScene(const CScene& s) const
{
    glm::mat4 m4view = cam1.matrix();
    glm::mat4 m4projView = m4proj() * m4view;

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    s1.use();
    s1.setMat4("m4projView", m4projView);
    s1.setVec3("viewPos", cam1.position());
    s1.setFloat("delta", delta());

    // material
    s1.tex2d("diffuseTexture", 0, 0);
    s1.tex2d("specularTexture", 0, 1);

    s.drawScene(
        [this](CScene::CNode* pNode) {
            s1.setMat4("m4model", pNode->matrix());
        });

    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
}









