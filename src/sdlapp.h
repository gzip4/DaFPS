#pragma once
#ifndef _DAFPS_SDLAPP_H
#define _DAFPS_SDLAPP_H

#include "active.h"
#include <list>
#include <fstream>
#include <memory>
#include <vector>
#include <SDL2/SDL.h>

#define GLSL(str)	"#version 330 core\n" #str

#define NEED_OPENGL true

class CInput;

class CSdlAppInit
{
public:
	CSdlAppInit(bool needOpenGL = false)
		: m_needOpenGL(needOpenGL)
	{
	}
	bool hasOpenGL() const { return m_needOpenGL; }

private:
	bool m_needOpenGL;
};


class CSdlApp
	: public virtual CSdlAppInit
	, public CActive
{
private:
	class _Window
	{
	private:
		friend class CSdlApp;
		bool m_focus{ false };
		bool m_resized{ false };
		int m_width{};
		int m_height{};
		SDL_Window* m_win{ nullptr };
		SDL_GLContext m_glcontext{ nullptr };

	public:
		SDL_Window* handle() const { return m_win; }
		SDL_GLContext glcontext() const { return m_glcontext; }
		bool hasFocus() const { return m_focus; }
		int width() const { return m_width; }
		int height() const { return m_height; }
		int resized() const { return m_resized; }
		float aspect() const
		{
			return (m_height == 0)
				? 1.0f
				: float(m_width) / float(m_height);
		}
		void swapWindow() const { SDL_GL_SwapWindow(m_win); }
	};

private:
	static CSdlApp* _sdlApp;
	_Window m_window;
	bool m_quit{ false };
	Uint32 m_ticks{};
	float m_delta{};
	std::unique_ptr<CInput> m_input{};
	std::list<CActive*> m_active{};

public:
	static CSdlApp& app() { return *_sdlApp; }
	CSdlApp(int argc, char* argv[]);
	~CSdlApp();

	void run();
	virtual void handleEvent(SDL_Event& event);
	virtual void idle();

	const _Window& window() const { return m_window; }
	CInput& input() const { return *m_input; }
	void quit() { m_quit = true; }
	bool isQuit() const { return m_quit; }
	Uint32 ticks() const { return m_ticks; }
	float delta() const { return m_delta; }
	void activeRegister(CActive* item);
	void activeUnregister(CActive* item);

	static void log(const std::string fmt, ...);

	struct io {
		static std::ifstream textInput(const std::string& filename);
		static std::ifstream binaryInput(const std::string& filename);
		static std::ofstream binaryOutput(const std::string& filename);
	};

	enum VSYNC {
		VSYNC_ADAPTIVE = -1,
		VSYNC_OFF = 0,
		VSYNC_ON = 1,
	};

private:
	void handleInput(SDL_Event& event);
	void handleWindowEvent(SDL_Event& event);
};

#endif // !_DAFPS_SDLAPP_H
