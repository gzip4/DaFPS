#pragma once
#ifndef _DAFPS_WRAPGL_H
#define _DAFPS_WRAPGL_H

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#if defined( __WIN32__ ) || defined( _WIN32 ) || defined( WIN32 )
#include <GL/glew.h>
#else
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>
#endif // _WIN32

namespace gl
{

	static inline void clearColor(glm::vec3 v, float a = 1.0f)
	{
		glClearColor(v.r, v.g, v.b, a);
	}

	static inline void clearColor(glm::vec4 v)
	{
		glClearColor(v.r, v.g, v.b, v.a);
	}

	static inline void clearAll()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	}

}

#endif // !_DAFPS_WRAPGL_H
