#pragma once
#ifndef _DAFPS_SCENE_H
#define _DAFPS_SCENE_H

#include "buffers.h"
#include <functional>
#include <cstdint>
#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <fx/gltf.h>

class CScene
{
public:
	//=====================================================================
	class CPrimitive
	{
		friend class CScene;
		CVertexArray m_vao{};
		uintptr_t m_indexOffset{};
		uint32_t m_indexCount{};
		uint8_t m_mode{ 4 };	// GL_TRIANGLES
		uint16_t m_componentType{};
		bool m_indexed{ false };
		std::vector<uint32_t> m_attribs{};
		CPrimitive() = default;

		void drawPrimitive();

	public:
		~CPrimitive() = default;
		CPrimitive(const CPrimitive&) = delete;
		CPrimitive& operator=(const CPrimitive&) = delete;

		CVertexArray& vao() { return m_vao; }
		uintptr_t indexOffset() const { return m_indexOffset; }
		uint32_t indexCount() const { return m_indexCount; }
		uint8_t mode() const { return m_mode; }
		uint16_t componentType() const { return m_componentType; }
		bool indexed() const { return m_indexed; }
		std::vector<uint32_t> attribs() const { return m_attribs; }
	};

	//=====================================================================
	class CMesh
	{
		friend class CScene;
		std::string m_name{};
		using Primitives = std::vector<std::unique_ptr<CPrimitive>>;
		Primitives m_primitives{};
		CMesh(const std::string& name);

	public:
		virtual ~CMesh();
		CMesh(const CMesh&) = delete;
		CMesh& operator=(const CMesh&) = delete;
		const Primitives& primitives() const
		{
			return m_primitives;
		}
	};

	//=====================================================================
	class CNode
	{
		friend class CScene;
		std::string m_name{};
		int32_t m_meshIndex{ -1 };
		glm::vec3 m_scale{ 1.0f };
		glm::vec3 m_translation{};
		glm::quat m_rotation{};
		glm::mat4 m_matrix{ glm::mat4(1.0f) };
		CNode(const std::string& name);

	public:
		virtual ~CNode();
		int32_t meshIndex() const { return m_meshIndex; }
		glm::vec3& scale() { return m_scale; }
		glm::vec3& translation() { return m_translation; }
		glm::quat& rotation() { return m_rotation; }
		glm::mat4& matrix() { return m_matrix; }
	};

	//=====================================================================
private:
	using Nodes = std::vector<std::unique_ptr<CNode>>;
	using Meshes = std::vector<std::unique_ptr<CMesh>>;
	Nodes m_nodes{};
	Meshes m_meshes{};

public:
	CScene() = default;
	virtual ~CScene() = default;
	CScene(const CScene&) = delete;
	CScene& operator=(const CScene&) = delete;
	using Buffers = std::vector<std::unique_ptr<CBuffer>>;
	using Document = fx::gltf::Document;
	void loadScene(const Document&);

	using Func1 = std::function<void(CNode*)>;
	using Func2 = std::function<void(CPrimitive*)>;
	void drawScene(Func1 = [](CNode*) {}, Func2 = [](CPrimitive*) {}) const;

private:
	void loadNode(uint32_t inode, const Document&, CNode* parent);
	void loadMesh(int32_t imesh, const Document&, Buffers&);

	static const std::unordered_map<std::string, uint8_t> attributeLocations()
	{
		return {
			{ "POSITION", 0 },
			{ "NORMAL", 1 },
			{ "TANGENT", 2 },
			{ "TEXCOORD_0", 3 },
			{ "TEXCOORD_1", 4 },
			{ "COLOR_0", 5 },
			{ "COLOR_1", 6 },
		};
	}
};

#endif // !_DAFPS_SCENE_H
