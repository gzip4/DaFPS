//VERTEX SHADER
#version 330 core

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec2 inTexCoord0;

out vec2 texCoord;

void main() {
    texCoord = inTexCoord0;
    gl_Position = inPosition;
}
