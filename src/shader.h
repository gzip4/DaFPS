#pragma once
#ifndef _DAFPS_SHADER_H
#define _DAFPS_SHADER_H

#include "sdlapp.h"
#include <cassert>
#include <string>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat2x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>

#if defined( __WIN32__ ) || defined( _WIN32 ) || defined( WIN32 )
#include <GL/glew.h>
#else
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>
#endif // _WIN32


class CShader {
protected:
    unsigned int m_prg{};

    enum class EStatusType
    {
        COMPILE,
        LINK,
        VALIDATE
    };

public:
    CShader(const CShader&) = delete;
    CShader& operator=(const CShader&) = delete;
    explicit CShader(unsigned int prg = 0);
    CShader(const std::string& vertex, const std::string& fragment);
    virtual ~CShader();
    static void unbind();

    void init(const std::string& vertex, const std::string& fragment);
    bool validate() const;
    void use() const
    {
        assert(m_prg != 0);
        glUseProgram(m_prg);
    }

    using Name = const std::string&;

    int uniform(Name name) const
    {
        assert(m_prg != 0);
        return glGetUniformLocation(m_prg, name.data());
    }

    void setInt(Name name, int value) const { glUniform1i(uniform(name), value); }
    void setFloat(Name name, float value) const { glUniform1f(uniform(name), value); }
    void setVec2(Name name, float x, float y) const { glUniform2f(uniform(name), x, y); }
    void setVec2(Name name, const glm::vec2& v2) const { glUniform2fv(uniform(name), 1, &v2[0]); }
    void setVec3(Name name, float x, float y, float z) const { glUniform3f(uniform(name), x, y, z); }
    void setVec3(Name name, const glm::vec3& v3) const { glUniform3fv(uniform(name), 1, &v3[0]); }
    void setVec4(Name name, float x, float y, float z, float w) const { glUniform4f(uniform(name), x, y, z, w); }
    void setVec4(Name name, const glm::vec4& v4) const { glUniform4fv(uniform(name), 1, &v4[0]); }
    void setMat2(Name name, const glm::mat2& m2) const { glUniformMatrix2fv(uniform(name), 1, GL_FALSE, &m2[0][0]); }
    void setMat3(Name name, const glm::mat3& m3) const { glUniformMatrix3fv(uniform(name), 1, GL_FALSE, &m3[0][0]); }
    void setMat4(Name name, const glm::mat4& m4) const { glUniformMatrix4fv(uniform(name), 1, GL_FALSE, &m4[0][0]); }

    void tex2d(Name name, unsigned int texture, unsigned int unit = 0) const
    {
        glActiveTexture(GL_TEXTURE0 + unit);
        glBindTexture(GL_TEXTURE_2D, texture);
        setInt(name, unit);
    }

    void texCube(Name name, unsigned int texture, unsigned int unit = 0) const
    {
        glActiveTexture(GL_TEXTURE0 + unit);
        glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
        setInt(name, unit);
    }


private:
    bool _init(const std::string& vertex, const std::string& fragment);
    bool _compile(unsigned int id, const std::string& source) const;
    bool _link(unsigned int prg, unsigned int vid, unsigned int fid) const;
    bool _checkStatus(unsigned int id, EStatusType) const;
};

#endif // !_DAFPS_SHADER_H
