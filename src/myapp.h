#pragma once
#ifndef _DAFPS_MYAPP_H
#define _DAFPS_MYAPP_H

#include "shader.h"
#include "sdlapp.h"
#include "input.h"
#include "camera.h"
#include "buffers.h"
#include "skybox.h"
#include "terrain.h"
#include "scene.h"
#include "resource.h"
#include "util.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <memory>
#include <sstream>
#include <vector>


#define RES_SHADER_TEST_VERT        "res://shaders/test_vert.glsl"
#define RES_SHADER_TEST_FRAG        "res://shaders/test_frag.glsl"
#define RES_SHADER_TEXTURE_VERT     "res://shaders/texture_vert.glsl"
#define RES_SHADER_TEXTURE_FRAG     "res://shaders/texture_frag.glsl"


class CMyApp : public CSdlApp
{
private:
    CShader s1{};
    CShader s2{};
    CFlyCamera2 cam1{};
    CSkybox skybox{};
    CTerrain terrain{};
    float m_fov{ 45.0f }; // degrees
    float alpha{};
    glm::mat4 m_m4proj{ 1.0f };
    CScene scene1{};
    bool m_consoleVisible{ false };

    struct
    {
        CVertexArray vao1{};
        CFrameBuffer fbo{};
    } buffers;

    enum class ERenderType {
        Color,
        Depth,
        ColorDepth
    };
    ERenderType m_rt{ ERenderType::Color };


    enum {
        WIN_H = 720,
        WIN_W = 1280
    };

public:
    CMyApp(int argc, char* argv[]);
    ~CMyApp();
    virtual void handleEvent(SDL_Event& event) override;
    virtual void idle() override;
    virtual void process() override;

private:
    const glm::mat4& m4proj() const { return m_m4proj; }
    void renderToTexture();
    void renderScene(const CScene&) const;
    void drawTextures(unsigned int tex1, unsigned int tex2);
};

#endif // !_DAFPS_MYAPP_H
