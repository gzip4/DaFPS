#pragma once
#ifndef _DAFPS_ACTIVE_H
#define _DAFPS_ACTIVE_H

class CActive
{
	bool m_reg;
public:
	CActive(bool reg = true);
	~CActive();
	virtual void process() = 0;
};

#endif // !_DAFPS_ACTIVE_H
