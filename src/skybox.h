#pragma once
#ifndef _DAFPS_SKYBOX_H
#define _DAFPS_SKYBOX_H

#include "shader.h"
#include "buffers.h"
#include <array>
#include <glm/mat4x4.hpp>


class CSkybox
{
private:
	unsigned int m_cubemap{};
	CVertexArray m_vao{};
	CShader m_shader{};

public:
	CSkybox();
	~CSkybox();
	CSkybox(const CSkybox&) = delete;
	CSkybox& operator=(const CSkybox&) = delete;

	void loadCubemap(const std::array<std::string, 6> &);
	void renderSkybox(const glm::mat4& mvp);
};

#endif // !_DAFPS_SKYBOX_H
