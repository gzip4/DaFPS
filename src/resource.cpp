#include "resource.h"
#include "sdlapp.h"
#include "util.h"
#include <cassert>
#include <fstream>
#include <sstream>
#include <iostream>
#include <cstdlib>


static std::string resourceIdToFile(const std::string& id)
{
	return std::string("res/") + id.substr(6);
}

std::ifstream Resource::binary() const
{
	return CSdlApp::io::binaryInput(resourceIdToFile(m_resId));
}

Resource::Resource(const std::string& resId)
	: m_resId{ resId }
{
	assert(resId.size() > 6 && util::startsWith(resId, "res://"));
}

std::string Resource::str() const
{
	std::ifstream ifs{ CSdlApp::io::textInput(resourceIdToFile(m_resId)) };
	std::string line{};
	std::stringstream ss{};
	while (getline(ifs, line)) {
		ss << line << std::endl;
	}

	return ss.str();
}

std::vector<unsigned char> Resource::bytes(size_t offset) const
{
	std::ifstream ifs{ binary() };

	ifs.seekg(0, ifs._Seekend);
	size_t fileSz = ifs.tellg();
	if (offset >= fileSz) return std::vector<unsigned char>();
	ifs.seekg(offset, ifs._Seekbeg);
	fileSz -= offset;
	std::vector<unsigned char> v1(fileSz);
	ifs.read(reinterpret_cast<char*>(&v1[0]), fileSz);

	if (!ifs)
	{
		CSdlApp::log("Cannot read resource: %s", m_resId.c_str());
		std::exit(18);
	}

	return v1;
}
