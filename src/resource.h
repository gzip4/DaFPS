#pragma once
#ifndef _DAFPS_RESOURCE_H
#define _DAFPS_RESOURCE_H

#include <string>
#include <vector>
#include <fstream>

class Resource
{
private:
	std::string m_resId;

	std::ifstream binary() const;

public:
	Resource(const std::string& resId);

	std::string id() const { return m_resId; }
	std::string str() const;
	std::vector<unsigned char> bytes(size_t offset = 0) const;

	template <typename T>
	std::vector<T> elements(size_t offset = 0) const
	{
		std::ifstream ifs{ binary() };
		ifs.seekg(0, ifs._Seekend);
		size_t fileSz = ifs.tellg();
		if (offset * sizeof(T) >= fileSz) return std::vector<T>();
		ifs.seekg(offset * sizeof(T), ifs._Seekbeg);
		fileSz -= offset * sizeof(T);

		unsigned int pad = fileSz % sizeof(T);
		if (pad) fileSz += sizeof(T) - pad;

		std::vector<T> v1(fileSz);
		ifs.read(reinterpret_cast<char*>(&v1[0]), fileSz);

		return v1;
	}
};

static inline std::ostream& operator<<(std::ostream& os, const Resource& r)
{
	os << "[" << r.id() << "]";
	return os;
}

#endif // !_DAFPS_RESOURCE_H
