#include "skybox.h"
#include "sdlapp.h"
#include "SOIL2/SOIL2.h"
#include <cassert>
#include <cstdlib>

#define GLSL(str)	"#version 330 core\n" #str


static const char _vertexShader[] = GLSL(
layout(location = 0) in vec3 inPosition;
out vec3 texCoord;
uniform mat4 m4mvp;
void main() {
	texCoord = inPosition;
	gl_Position = (m4mvp * vec4(inPosition, 1.0)).xyww;
});

static const char _fragmentShader[] = GLSL(
in vec3 texCoord;
out vec4 fragColor;
uniform samplerCube cubemap;
void main() {
	fragColor = texture(cubemap, texCoord);
});


static const float mesh[] = {
	-1.0f, 1.0f, 1.0f, 0.333333f, 0.333333f,
	-1.0f, -1.0f, -1.0f, 0.666667f, 0.0f,
	-1.0f, -1.0f, 1.0f, 0.666667f, 0.333333f,
	-1.0f, 1.0f, -1.0f, 0.0f, 0.666667f,
	1.0f, -1.0f, -1.0f, 0.333333f, 0.333333f,
	-1.0f, -1.0f, -1.0f, 0.333333f, 0.666667f,
	1.0f, 1.0f, -1.0f, 0.0f, 0.333333f,
	1.0f, -1.0f, 1.0f, 0.333333f, 0.0f,
	1.0f, -1.0f, -1.0f, 0.333333f, 0.333333f,
	1.0f, 1.0f, 1.0f, 0.333333f, 0.333333f,
	-1.0f, -1.0f, 1.0f, 0.666667f, 0.666667f,
	1.0f, -1.0f, 1.0f, 0.333333f, 0.666667f,
	1.0f, -1.0f, -1.0f, 0.666667f, 0.333333f,
	-1.0f, -1.0f, 1.0f, 1.0f, 0.0f,
	-1.0f, -1.0f, -1.0f, 1.0f, 0.333333f,
	-1.0f, 1.0f, -1.0f, 0.333333f, 1.0f,
	1.0f, 1.0f, 1.0f, 0.0f, 0.666667f,
	1.0f, 1.0f, -1.0f, 0.333333f, 0.666667f,
	-1.0f, 1.0f, 1.0f, 0.333333f, 0.333333f,
	-1.0f, 1.0f, -1.0f, 0.333333f, 0.0f,
	-1.0f, -1.0f, -1.0f, 0.666667f, 0.0f,
	-1.0f, 1.0f, -1.0f, 0.0f, 0.666667f,
	1.0f, 1.0f, -1.0f, 0.0f, 0.333333f,
	1.0f, -1.0f, -1.0f, 0.333333f, 0.333333f,
	1.0f, 1.0f, -1.0f, 0.0f, 0.333333f,
	1.0f, 1.0f, 1.0f, 0.0f, 0.0f,
	1.0f, -1.0f, 1.0f, 0.333333f, 0.0f,
	1.0f, 1.0f, 1.0f, 0.333333f, 0.333333f,
	-1.0f, 1.0f, 1.0f, 0.666667f, 0.333333f,
	-1.0f, -1.0f, 1.0f, 0.666667f, 0.666667f,
	1.0f, -1.0f, -1.0f, 0.666667f, 0.333333f,
	1.0f, -1.0f, 1.0f, 0.666667f, 0.0f,
	-1.0f, -1.0f, 1.0f, 1.0f, 0.0f,
	-1.0f, 1.0f, -1.0f, 0.333333f, 1.0f,
	-1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 0.0f, 0.666667f
};


CSkybox::CSkybox()
	: m_shader{ _vertexShader, _fragmentShader }
{
	CBuffer b{};
	m_vao.bind();
	b.data(sizeof(mesh), mesh);
	m_vao.attrV3(0, 5 * sizeof(float));
	m_vao.attrUV(1, 5 * sizeof(float), 3 * sizeof(float));
	m_vao.unbind();
}

CSkybox::~CSkybox()
{
	if (m_cubemap) glDeleteTextures(1, &m_cubemap);
}

/**
 * \brief Load cubemap
 *
 * \param textures	- list of filenames: pos_x, neg_x, pos_y, neg_y,
 *                  pos_z, neg_z
 *
 */
void CSkybox::loadCubemap(const std::array<std::string, 6> & textures)
{
	m_cubemap = SOIL_load_OGL_cubemap(
		textures[0].data(),		// xp
		textures[1].data(),		// xn
		textures[2].data(),		// yp
		textures[3].data(),		// yn
		textures[4].data(),		// zp
		textures[5].data(),		// zn
		SOIL_LOAD_RGB,
		(m_cubemap == 0) ? SOIL_CREATE_NEW_ID : m_cubemap,
		SOIL_FLAG_DDS_LOAD_DIRECT);

	if (!m_cubemap)
	{
		CSdlApp::log("Error loadling skybox textures: %s", SOIL_last_result());
		exit(3);
	}
}

void CSkybox::renderSkybox(const glm::mat4& mvp)
{
	if (!m_cubemap) return;

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
	glDepthFunc(GL_LEQUAL);

	m_shader.use();
	m_shader.setMat4("m4mvp", mvp);
	m_shader.texCube("cubemap", m_cubemap);
	{
		m_vao.bind();
		m_vao.enableAttr({ 0, 1 });
		glDrawArrays(GL_TRIANGLES, 0, 36);
		m_vao.disableAttr({ 0, 1 });
		m_vao.unbind();
	}
	m_shader.unbind();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
}
