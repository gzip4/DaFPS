#include "staticmesh.h"

CStaticMesh::CStaticMesh()
	: m_pos(0.0f)
	, m_rot(0.0f)
	, m_scale(1.0f)
	, m_vao()
	, m_lod(MAX_LOD)
{
}

CStaticMesh::~CStaticMesh()
{
}

void CStaticMesh::init()
{
}

void CStaticMesh::initAttrib(CVertexArray const& vao)
{
	//struct SAttr
	//{
	//	glm::vec3 pos;
	//	glm::vec3 normal;
	//	glm::vec3 color;
	//	glm::vec2 uv0;
	//	glm::vec2 uv1;
	//};

	uintptr_t off = 0;
	const auto stride = sizeof(SAttr);
	std::vector<int> size({3, 3, 3, 2, 2});
	for (int i = 0; i < size.size(); i++)
	{
		vao.attrV(i, size[i], stride, off);
		off += (size[i] == 3) ? sizeof(glm::vec3) : sizeof(glm::vec2);
	}

	//vao.attribute(0, 3, stride, off); off += sizeof(glm::vec3);
	//vao.attribute(1, 3, stride, off); off += sizeof(glm::vec3);
	//vao.attribute(2, 3, stride, off); off += sizeof(glm::vec3);
	//vao.attribute(3, 2, stride, off); off += sizeof(glm::vec2);
	//vao.attribute(4, 2, stride, off); off += sizeof(glm::vec2);
}
