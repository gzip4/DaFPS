#include "buffers.h"

#if defined( __WIN32__ ) || defined( _WIN32 ) || defined( WIN32 )
#include <GL/glew.h>
#else
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>
#endif // _WIN32


CFrameBuffer::~CFrameBuffer()
{
	if (mcolor) glDeleteTextures(1, &mcolor);
	if (mdepth) glDeleteTextures(1, &mdepth);
	if (mstencil) glDeleteTextures(1, &mstencil);
	if (id) glDeleteFramebuffers(1, &id);
}

void CFrameBuffer::attachColor(int w, int h)
{
	bind();

	if (mcolor) detachColor();

	glGenTextures(1, &mcolor);
	glBindTexture(GL_TEXTURE_2D, mcolor);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mcolor, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void CFrameBuffer::detachColor()
{
	if (mcolor)
	{
		bind();
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
		glDeleteTextures(1, &mcolor);
		mcolor = 0;
	}
}

void CFrameBuffer::attachDepth(int w, int h)
{
	bind();

	if (mdepth) detachDepth();

	glGenTextures(1, &mdepth);
	glBindTexture(GL_TEXTURE_2D, mdepth);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, w, h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, mdepth, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void CFrameBuffer::detachDepth()
{
	if (mdepth)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
		glDeleteTextures(1, &mdepth);
		mdepth = 0;
	}
}

void CFrameBuffer::attachStencil(int w, int h)
{
	bind();

	if (mstencil) detachStencil();

	glGenTextures(1, &mstencil);
	glBindTexture(GL_TEXTURE_2D, mstencil);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_STENCIL_COMPONENTS, w, h, 0, GL_STENCIL_COMPONENTS, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, mstencil, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void CFrameBuffer::detachStencil()
{
	if (mstencil)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, 0, 0);
		glDeleteTextures(1, &mstencil);
		mstencil = 0;
	}
}
