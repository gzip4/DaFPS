#pragma once
#ifndef _DAFPS_INPUT_H
#define _DAFPS_INPUT_H

#include <array>
#include <cstdint>

class CInput
{
	friend class CSdlApp;
	enum
	{
		NBUTTONS = 32,
		NKEYS = 1024,
	};

	typedef uint64_t array_type;

	std::array<array_type, NKEYS / (sizeof(array_type) * 8)> m_keys;
	array_type m_buttons{};
	int m_xrel{};
	int m_yrel{};

	void keyEvent(int scancode, bool state);
	void buttonEvent(int n, bool state);
	void motionEvent(int xrel, int yrel);

public:
	CInput();
	CInput(const CInput&) = delete;
	CInput& operator=(const CInput&) = delete;

	bool keyPressed(int scancode) const;
	bool buttonPressed(int n) const;
	int xrel() const { return m_xrel; }
	int yrel() const { return m_yrel; }
};

#endif // !_DAFPS_INPUT_H
