#pragma once
#ifndef _DAFPS_BUFFERS_H
#define _DAFPS_BUFFERS_H

#include <cstdint>
#include <vector>

#if defined( __WIN32__ ) || defined( _WIN32 ) || defined( WIN32 )
#include <GL/glew.h>
#else
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>
#endif // _WIN32


class CAbstractBuffer
{
protected:
	uint32_t id{};

public:
	explicit CAbstractBuffer(uint32_t _id = 0);
	virtual ~CAbstractBuffer() {}
	CAbstractBuffer(CAbstractBuffer const&) = delete;
	CAbstractBuffer& operator=(CAbstractBuffer const&) = delete;
};


class CBuffer : public CAbstractBuffer
{
public:
	enum Target
	{
		Array = GL_ARRAY_BUFFER,
		Index = GL_ELEMENT_ARRAY_BUFFER
	};

	using CAbstractBuffer::CAbstractBuffer;
	virtual ~CBuffer() override;
	static void unbind(Target target = Array);
	void bind(Target target = Array);
	void data(ptrdiff_t size, const void* data = nullptr, Target target = Array, uint32_t usage = GL_STATIC_DRAW);
	template<typename T>
	void data(std::vector<T> const&, Target target = Array, uint32_t usage = GL_STATIC_DRAW);
};


class CVertexArray : public CAbstractBuffer
{
public:
	using CAbstractBuffer::CAbstractBuffer;
	virtual ~CVertexArray() override;
	static void unbind();
	void bind();
	void attrV(uint32_t index, int size, int stride = 0,
		std::uintptr_t offset = 0) const;
	void attrV2(uint32_t index, int stride = 0,
		std::uintptr_t offset = 0) const;
	void attrV3(uint32_t index, int stride = 0,
		std::uintptr_t offset = 0) const;
	void attrV4(uint32_t index, int stride = 0,
		std::uintptr_t offset = 0) const;
	void attrUV(uint32_t index, int stride = 0,
		std::uintptr_t offset = 0) const;

	void attrColor(uint32_t index, uint32_t type, int stride = 0,
		std::uintptr_t offset = 0) const;
	void attrColor8(uint32_t index, int stride = 0,
		std::uintptr_t offset = 0) const;
	void attrColor16(uint32_t index, int stride = 0,
		std::uintptr_t offset = 0) const;
	void attrColor32(uint32_t index, int stride = 0,
		std::uintptr_t offset = 0) const;

	void enableAttr(uint32_t);
	void disableAttr(uint32_t);
	void enableAttr(const std::vector<uint32_t>&);
	void disableAttr(const std::vector<uint32_t>&);
};


class CRenderBuffer : public CAbstractBuffer
{
public:
	using CAbstractBuffer::CAbstractBuffer;
	virtual ~CRenderBuffer() override;
	static void unbind();
	void bind();
	void storage(int w, int h, uint32_t format);
};


class CFrameBuffer : public CAbstractBuffer
{
protected:
	uint32_t mcolor{};
	uint32_t mdepth{};
	uint32_t mstencil{};

public:
	using CAbstractBuffer::CAbstractBuffer;
	virtual ~CFrameBuffer() override;
	static void unbind();
	void bind();
	void attachColor(int w, int h);
	void detachColor();
	void attachDepth(int w, int h);
	void detachDepth();
	void attachStencil(int w, int h);
	void detachStencil();
	bool check();

	uint32_t color() { return mcolor; }
	uint32_t depth() { return mdepth; }
	uint32_t stencil() { return mstencil; }
};


///////////////////////////////////////////////////////////////////////////
// INLINE IMPLEMENTATION
///////////////////////////////////////////////////////////////////////////

inline CAbstractBuffer::CAbstractBuffer(uint32_t _id)
	: id(_id)
{
}

inline CBuffer::~CBuffer()
{
	if (id) glDeleteBuffers(1, &id);
}

inline void CBuffer::bind(Target target)
{
	if (!id) glGenBuffers(1, &id);
	glBindBuffer(target, id);
}

inline void CBuffer::unbind(Target target)
{
	glBindBuffer(target, 0);
}

inline void CBuffer::data(ptrdiff_t size, const void* pdata,
	Target target, uint32_t usage)
{
	bind(target);
	glBufferData(target, size, pdata, usage);
}

template<typename T>
inline void
CBuffer::data(const std::vector<T>& v, Target target, uint32_t usage)
{
	bind(target);
	glBufferData(
		target,
		v.size() * sizeof(std::vector<T>::value_type),
		v.data(),
		usage);
}

inline CVertexArray::~CVertexArray()
{
	if (id) glDeleteVertexArrays(1, &id);
}

inline void CVertexArray::bind()
{
	if (!id) glGenVertexArrays(1, &id);
	glBindVertexArray(id);
}

inline void
CVertexArray::attrV(uint32_t index, int size,
	int stride, std::uintptr_t offset) const
{
	glVertexAttribPointer(index, size, GL_FLOAT, GL_FALSE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::attrV2(uint32_t index, int stride,
	std::uintptr_t offset) const
{
	glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::attrV3(uint32_t index, int stride,
	std::uintptr_t offset) const
{
	glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::attrV4(uint32_t index, int stride,
	std::uintptr_t offset) const
{
	glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::attrUV(uint32_t index, int stride,
	std::uintptr_t offset) const
{
	glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::attrColor(uint32_t index, uint32_t type,
	int stride, std::uintptr_t offset) const
{
	glVertexAttribPointer(index, 4, type, GL_TRUE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::attrColor8(uint32_t index, int stride,
	std::uintptr_t offset) const
{
	glVertexAttribPointer(index, 4, GL_UNSIGNED_BYTE, GL_TRUE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::attrColor16(uint32_t index, int stride,
	std::uintptr_t offset) const
{
	glVertexAttribPointer(index, 4, GL_UNSIGNED_SHORT, GL_TRUE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::attrColor32(uint32_t index, int stride,
	std::uintptr_t offset) const
{
	glVertexAttribPointer(index, 4, GL_UNSIGNED_INT, GL_TRUE, stride,
		reinterpret_cast<const void*>(offset));
}

inline void CVertexArray::unbind()
{
	glBindVertexArray(0);
}

inline void CVertexArray::enableAttr(uint32_t index)
{
	glEnableVertexAttribArray(index);
}

inline void CVertexArray::disableAttr(uint32_t index)
{
	glDisableVertexAttribArray(index);
}

inline void CVertexArray::enableAttr(const std::vector<uint32_t>& arrays)
{
	for (auto index : arrays)
		glEnableVertexAttribArray(index);
}

inline void CVertexArray::disableAttr(const std::vector<uint32_t>& arrays)
{
	for (auto index : arrays)
		glDisableVertexAttribArray(index);
}

inline CRenderBuffer::~CRenderBuffer()
{
	if (id) glDeleteRenderbuffers(1, &id);
}

inline void CRenderBuffer::bind()
{
	if (!id) glGenRenderbuffers(1, &id);
	glBindRenderbuffer(GL_RENDERBUFFER, id);
}

inline void CRenderBuffer::unbind()
{
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

inline void CRenderBuffer::storage(int w, int h, uint32_t format)
{
	bind();
	glRenderbufferStorage(GL_RENDERBUFFER, format, w, h);
}

inline void CFrameBuffer::bind()
{
	if (!id) glGenFramebuffers(1, &id);
	glBindFramebuffer(GL_FRAMEBUFFER, id);
}

inline void CFrameBuffer::unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

inline bool CFrameBuffer::check()
{
	bind();
	return glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE;
}

#endif // !_DAFPS_BUFFERS_H
