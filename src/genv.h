#pragma once
#ifndef _DAFPS_GENV_H
#define _DAFPS_GENV_H

#include <SDL2/SDL.h>

struct _genv_s {
	struct {
		SDL_Window* win;
		SDL_GLContext glcontext;
	} sdl;
};

extern _genv_s genv;

#endif // !_DAFPS_GENV_H
