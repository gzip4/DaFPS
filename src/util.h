#pragma once
#ifndef _DAFPS_UTIL_H
#define _DAFPS_UTIL_H

#include "resource.h"
#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>
#include <glm/gtx/io.hpp>

namespace util
{

    static inline bool startsWith(const std::string& s, const std::string& prefix)
    {
        if (prefix.length() > s.length()) return false;
        //return std::equal(prefix.begin(), prefix.end(), s.begin());
        return s.find(prefix) == 0;
    }

    template <typename T>
    static inline std::string str(const T& m)
    {
        std::stringstream ss{};
        ss << m;
        return ss.str();
    }

    template <>
    static inline std::string str(const ::Resource& r)
    {
        return r.str();
    }


    constexpr size_t npos = static_cast<size_t>(-1);

    static inline size_t fileSize(const std::string& fn)
    {
        std::ifstream fs(fn);
        if (!fs) return npos;
        fs.seekg(0, fs._Seekend);
        return fs.tellg();
    }

} // namespace util

#endif // !_DAFPS_UTIL_H
