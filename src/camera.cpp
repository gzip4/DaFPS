#include "camera.h"
#include "sdlapp.h"
#include "input.h"
#include <glm/gtc/matrix_transform.hpp>


glm::mat4 CCamera::rotationMatrix() const
{
	glm::mat4 m{ 1.0f };
	m = glm::rotate(m, rot.x, glm::vec3(1.f, 0.f, 0.f));
	m = glm::rotate(m, rot.y, glm::vec3(0.f, 1.f, 0.f));
	m = glm::rotate(m, rot.z, glm::vec3(0.f, 0.f, 1.f));
	return m;
}

glm::mat4 CCamera::matrix() const
{
	return glm::translate(rotationMatrix(), pos);
}

void CFlyCamera::handleEvent(SDL_Event& e)
{
	const auto& input = CSdlApp::app().input();

	if (e.type == SDL_MOUSEMOTION && SDL_GetRelativeMouseMode())
	{
		yaw() += float(input.xrel()) * m_xsensitivity;
		pitch() += float(input.yrel()) * m_ysensitivity;
	}
}

static void processWSAD(const CInput& input, glm::vec3& dir, glm::vec3& up)
{
	if (input.keyPressed(SDL_SCANCODE_W)) dir.z += 1.0f;
	if (input.keyPressed(SDL_SCANCODE_S)) dir.z -= 1.0f;
	if (input.keyPressed(SDL_SCANCODE_A)) dir.x += 1.0f;
	if (input.keyPressed(SDL_SCANCODE_D)) dir.x -= 1.0f;
	if (input.keyPressed(SDL_SCANCODE_Q)) up.y += 1.0f;
	if (input.keyPressed(SDL_SCANCODE_E)) up.y -= 1.0f;
}

void CFlyCamera::process()
{
	const auto& input = CSdlApp::app().input();
	const float delta = CSdlApp::app().delta();

	glm::vec3 dir{ 0.0f };
	glm::vec3 up{ 0.0f };
	processWSAD(input, dir, up);

	//float breath_x = 0.1f * delta * sinf(m_angle / 6.0f);
	//float breath_y = 0.1f * delta * cosf(m_angle / 8.0f);
	//m_camera->pitch() += breath_x;
	//m_camera->yaw() += breath_y;

	if (dir.x != 0.0f || dir.z != 0.0f)
	{
		// vector * matrix - order matters
		glm::vec3 offset(glm::vec4(dir, 0.0f) * rotationMatrix());
		move(offset * maxSpeed() * delta);
	}

	if (up.y != 0.0f) move(up * maxSpeed() * delta);
}

void CFlyCamera2::process()
{
	const auto& input = CSdlApp::app().input();
	const float delta = CSdlApp::app().delta();

	glm::vec3 dir{ 0.0f };
	glm::vec3 up{ 0.0f };
	processWSAD(input, dir, up);

	if (dir.x != 0.0f || dir.z != 0.0f)
	{
		m_vel += dir * m_accel * delta;
		if (glm::length(m_vel) > maxSpeed())
		{
			m_vel = glm::normalize(m_vel) * maxSpeed();
		}
	}
	else
	{
		m_vel -= m_vel / 4.0f;
		if (glm::length(m_vel) < 0.01f) m_vel = glm::vec3(0.0f);
	}

	// vector * matrix - order matters
	glm::vec3 offset(glm::vec4(m_vel, 0.0f) * rotationMatrix());
	move(offset * delta);

	if (up.y != 0.0f) move(up * maxSpeed() * delta);
}
