#include "input.h"
#include <cassert>


CInput::CInput()
{
	m_keys.fill(0);
}

void CInput::keyEvent(int sc, bool state)
{
	assert(sc >= 0 && sc < NKEYS);
	const int bits = sizeof(array_type) * 8;
	const int i = sc / bits;
	const int j = sc % bits;

	if (state)
		m_keys[i] |= array_type(1) << j;
	else
		m_keys[i] &= ~(array_type(1) << j);
}

void CInput::buttonEvent(int n, bool state)
{
	assert(n >= 0 && n < NBUTTONS);
	if (state)
		m_buttons |= array_type(1) << n;
	else
		m_buttons &= ~(array_type(1) << n);
}

void CInput::motionEvent(int xrel, int yrel)
{
	m_xrel = xrel;
	m_yrel = yrel;
}

bool CInput::keyPressed(int sc) const
{
	assert(sc >= 0 && sc < NKEYS);
	const int bits = sizeof(array_type) * 8;
	const int i = sc / bits;
	const int j = sc % bits;
	return m_keys[i] & (array_type(1) << j);
}

bool CInput::buttonPressed(int n) const
{
	assert(n >= 0 && n < NBUTTONS);
	return m_buttons & (array_type(1) << n);
}
