//VERTEX SHADER
#version 330 core

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inNormal;
layout(location = 2) in vec4 inTangent;
layout(location = 3) in vec2 inTexCoord0;
layout(location = 4) in vec2 inTexCoord1;
layout(location = 5) in vec4 inColor0;
layout(location = 6) in vec4 inColor1;

out vec4 fragPos;
out vec3 normalVector;
out vec2 texCoord0;
out vec4 color0;

uniform mat4 m4model;
uniform mat4 m4projView;
uniform float delta = 0.001;

void main() {
    texCoord0 = inTexCoord0;
    color0 = inColor0;
    fragPos = m4model * inPosition;
    normalVector = mat3(transpose(inverse(m4model))) * inNormal.xyz;

    gl_Position = m4projView * fragPos;
}
