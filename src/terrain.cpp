#include "terrain.h"
#include "sdlapp.h"
#include <cassert>
#include <cstdlib>
#include <fstream>

#define GLSL(str)	"#version 330 core\n" #str


static const char vtxShader[] = GLSL(
layout(location = 0) in vec2 in_position;
uniform mat4 m4mvp;
uniform sampler2D heightMap;
uniform float hmW = 1.0;
uniform float scale = 1.0;
out vec2 uv0;
void main() {
	uv0 = in_position / hmW;
	vec2 v = in_position - hmW / 2.0;
	vec4 height = texture(heightMap, uv0);
	float h = height.r * 25.0 - 12.0;
	vec3 v3 = vec3(v.x, h, v.y) * scale;
	gl_Position = m4mvp * vec4(v3, 1.0);
});

static const char frgShader[] = GLSL(
in vec2 uv0;
uniform vec4 color;
out vec4 fragColor;
uniform sampler2D heightMap;
void main() {
	vec4 h = texture(heightMap, uv0);
	fragColor = vec4(h.rgb, 0.5);
});

//fragColor = vec4(color.rgb * 1.0 * h.r, 0.5);
//fragColor = color;

CTerrain::CTerrain()
	: m_shader(vtxShader, frgShader)
{
}

CTerrain::~CTerrain()
{
	if (m_heightMap) glDeleteTextures(1, &m_heightMap);
}

void CTerrain::init(uint32_t dims)
{
	using Index = unsigned int;

	// 256*256=65536  ushort max
	// 512*512=262144
	// 1024*1024=1048576 - crash
	// 2048^2=4194304
	// 4096^2=16777216
	// 8192^2=67108864
	//assert(dims >= 8 && dims <= 256);

	m_dims = dims;

	const int w = dims;
	const int buffsz = sizeof(glm::vec2) * w * w;
	const int idxsz = sizeof(Index) * w * w * w;
	std::vector<glm::vec2> buff(buffsz);
	std::vector<Index> idx(idxsz);

	for (int j = 0; j < w; ++j) {
		for (int i = 0; i < w; ++i) {
			buff[j * w + i] = glm::vec2(float(i), float(j));
		}
	}

	int i, j, k = 0;
	for (j = 0; j < (w - 1); ++j) {
		if (j % 2 == 0) {
			for (i = 0; i < w; ++i) {
				idx[k++] = i + j * w;
				idx[k++] = i + j * w + w;
			}
			if (j != w - 2) {
				idx[k++] = --i + j * w;
			}
		}
		else {
			for (i = w - 1; i >= 0; --i) {
				idx[k++] = i + j * w;
				idx[k++] = i + j * w + w;
			}
			if (j != w - 2) {
				idx[k++] = ++i + j * w;
			}
		}
	}
	m_indexSize = k;

	{
		CBuffer b1, b2;
		m_vao.bind();
		b1.data(buff);
		b2.data(idx, CBuffer::Index);
		m_vao.attrV2(0);
		m_vao.unbind();
	}

	std::string fname{ "res/heightMap.raw" };
	std::ifstream f1 = CSdlApp::io::binaryInput(fname);
	std::vector<char> tdata(65536);
	if (!f1.read(tdata.data(), 65536))
	{
		CSdlApp::log("Cannot read file: %s", fname.c_str());
		exit(13);
	}

	glGenTextures(1, &m_heightMap);
	glBindTexture(GL_TEXTURE_2D, m_heightMap);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RG, 256, 256, 0,
		GL_RED, GL_UNSIGNED_BYTE, tdata.data());
	// glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void CTerrain::renderTerrain(const glm::mat4& mvp)
{
	//static float scale = 150.0f, scaleDir = 1.0f;
	float scale = 20.1f;

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	m_shader.use();
	m_shader.setMat4("m4mvp", mvp);
	m_shader.setVec4("color", 6.0f, 0.2f, 0.0f, 0.3f);
	m_shader.setFloat("hmW", float(m_dims));
	m_shader.setFloat("scale", scale);
	m_shader.tex2d("heightMap", m_heightMap);
	{
		m_vao.bind();
		m_vao.enableAttr(0);
		glDrawElements(GL_TRIANGLE_STRIP, m_indexSize, GL_UNSIGNED_INT, 0);
		//glDrawElements(GL_TRIANGLE_STRIP, m_indexSize, GL_UNSIGNED_SHORT, 0);
		m_vao.disableAttr(0);
		m_vao.unbind();
	}
	m_shader.unbind();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//scale += scaleDir * 0.2;
	//if (scale > 180.0f) scaleDir = -1.0f;
	//if (scale < 120.0f) scaleDir = 1.0f;
}
