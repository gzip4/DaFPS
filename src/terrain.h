#pragma once
#ifndef _DAFPS_TERRAIN_H
#define _DAFPS_TERRAIN_H

#include "shader.h"
#include "buffers.h"
#include <cstdint>
#include <glm/mat4x4.hpp>

class CTerrain
{
private:
	uint32_t m_dims{};
	uint32_t m_indexSize{};
	uint32_t m_heightMap{};
	CShader m_shader{};
	CVertexArray m_vao{};

public:
	CTerrain();
	~CTerrain();
	CTerrain(const CTerrain&) = delete;
	CTerrain& operator=(const CTerrain&) = delete;

	void init(uint32_t dims);
	void renderTerrain(const glm::mat4& mvp);
};

#endif // !_DAFPS_TERRAIN_H
